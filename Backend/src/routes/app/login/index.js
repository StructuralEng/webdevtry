const express = require('express');
const router = express.Router();
const loginController = require('../../../controllers/app/loginController')
const loginValidate = require('../../../validations/loginValidates');
router.post('/signup', loginValidate.validateSignUp, loginController.SignUp);

module.exports = router;