const express = require('express');
const router = express.Router();

loginRouter = require('./login/index.js');
router.use('/login', loginRouter);

module.exports = router;