const express = require('express');
const router = express.Router();

const adminRouter = require('./admin/index.js');
const appRouter = require('./app/index.js');
router.use('/admin', adminRouter);
router.use('/app', appRouter);
router.get('/', (req, res) => {
    res.send('<h1 style="color:red;">Welcome to home page!</h1>');
});
module.exports = router;