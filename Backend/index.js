const express = require('express');
const http = require('http');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

const app = express();
const server = http.createServer(app);
dotenv.config()

app.use(express.json());
app.use(express.urlencoded({ extended: true }));




const Routes = require('./src/routes/index.js');
app.use('/', Routes);

mongoose.connect(process.env.DATABASE_URL + process.env.DATABASE_NAME,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        // useCreateIndex: true
    });



server.listen(process.env.PORT, process.env.HOST, () => {
    console.log('Server ran successfully on port ' + process.env.PORT);
}) 